from elf import Elf
from present import Present
from sleigh import Sleigh
from toy_machine import ToyMachine


def test_tell_elf_to_load_present_to_sleigh():
    elf = Elf()
    sleigh = Sleigh()
    called = False

    def callback():
        nonlocal called
        called = True

    elf.load_present_to_sleigh(Present(), sleigh, callback)
    assert called is True
    assert len(sleigh.presents) == 1
    assert sleigh.presents[0] is not None


def test_toy_machine_gives_present_to_elf():
    machine = ToyMachine()
    elf = Elf()
    assert elf.present is None

    machine._give_present(elf)

    assert elf.present is not None


def test_elf_packs_present_onto_sleigh():
    elf = Elf()
    elf._take_present(Present())
    sleigh = Sleigh()

    elf._pack_onto(sleigh)

    assert len(sleigh.presents) == 1
    assert elf.present is None
