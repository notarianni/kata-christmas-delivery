from toy_machine import machine, toy_machine


def test_toy_machine_end_point():
    machine.config['TESTING'] = True
    called = None

    def fake_http_client(url, data):
        nonlocal called
        called = (url, data)
        return {}

    machine.http_client = fake_http_client

    client = machine.test_client()

    rv = client.post('/workers', data='{"url":"http://fake-url"}', content_type="application/json")
    assert 201 == rv.status_code
    assert called is None
    toy_machine.send_presents()
    assert ("http://fake-url", {'name': "My present"}) == called
