from present import Present


class Elf:

    def __init__(self):
        self.present = None

    def _take_present(self, present):
        self.present = present

    def _release_present(self) -> Present:
        present = self.present
        self.present = None
        return present

    def _pack_onto(self, sleigh):
        sleigh.receive(self._release_present())

    def load_present_to_sleigh(self, present, sleigh, callback):
        self._take_present(present)
        self._pack_onto(sleigh)
        callback()
