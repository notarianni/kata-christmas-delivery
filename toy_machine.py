from flask import Flask, request

from elf import Elf
from present import Present


class ToyMachine:

    def _give_present(self, elf: Elf) -> None:
        present = Present()
        elf._take_present(present)

    def add_elf(self, elf_callback):
        self.elf_callback = elf_callback

    def send_presents(self):
        self.elf_callback({"name":"My present"})

machine = Flask(__name__)

toy_machine = ToyMachine()


@machine.route('/workers', methods=['POST'])
def index():
    url = request.json['url']
    toy_machine.add_elf(lambda present: machine.http_client(url, present))
    return "Hello world !", 201

if __name__ == "__main__":
    machine.run()
