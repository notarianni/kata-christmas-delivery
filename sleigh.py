class Sleigh:
    def __init__(self):
        self._presents = []

    def receive(self, present):
        self._presents.append(present)

    @property
    def presents(self):
        return self._presents
